# Arch Linux main PC

Dotfiles for Arch Linux with i3-gaps, polybar, rofi & sxiv. more to be added.

This project contains dotfiles used on my main PC


# SCREENSHOTS
Screenshot 1:

![2019-04-08-221311_3440x1440_scrot](/uploads/a8e1c0b94a385f8318245f05ffc5013b/2019-04-08-221311_3440x1440_scrot.png)

Screenshot 2:
![2019-04-08-221839_3440x1440_scrot](/uploads/452c6016fb6922e6b7717755a31b43b8/2019-04-08-221839_3440x1440_scrot.png)


# PC SPECS

*  CPU:         Intel Core i7-8700K @4.700GHz
*  GPU:         MSI Gaming X GeForce GTX 1080 Ti
*  MB:          MSI Z370 Gaming M5
*  Memory:      4 x 8 HyperX Predator 4000MHz DDR4
*  CPU Cooler:  Corsair H115i Pro
*  PSU:         Corsair HX850i
*  HDD:         WD Blue 1TB
*  SSD:         Samsung 850 Evo
*  NVMe:        Samsung 970 Evo
*  PC case:     Thermaltake View 37 RGB

Other:
*  NZXT internal USB hub
*  2 x Corsair LL140 RGB fan + controller
*  Thermaltake TT Premium PCI-E Extender

Peripherals:
*  Monitor:     Dell Alienware AW3418DW (3440 x 1440)
*  Keyboard:    Roccat Vulcan 120 AIMO
*  Mouse:       Logitech G703 HERO
*  Mousepad:    Razer Goliathus Extended Chroma
*  Headphones:  Sennheiser HD660s
*  DAC/AMP:     FX-Audio DAC-X6



# SOFTWARE

*  Samsung 850 Evo: Arch Linux x86_64
*  Samsung 970 Evo: Windows 10 Pro
*  `Grub` as bootloader



# ARCH PACKAGES

*  `i3-gaps` as WM
*  `Polybar` as statusbar
*  `Nvidia` (pacman) as display driver
*  `URxvt` as terminal
*  `Feh` as wallpaper manager
*  `Sxiv` as wallpaper viewer
*  `Firefox` as browser
*  `Rofi` as dmenu
*  `Thunar` as file browser
*  `Xorg` as display server
*  `Compton` as composition manager


# THEME

*  `Source Code Pro` as main font (ttf-google-fonts-git)
*  `Font Awesome` as icon font
*  `Arc-Dark` as GTK2/3 theme
*  `Papirus-Dark` as GTK2/3 icons