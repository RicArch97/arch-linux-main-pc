#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# aliases
if [ -f ~/.bash_aliases ]; then
. ~/.bash_aliases
fi
