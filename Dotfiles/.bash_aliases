# aliases file

# school folder in harddrive
alias school="cd / && cd /run/media/ricardo/Seagate\ Expansion\ Drive/School/ && ls"

# system
alias update="sudo pacman -Syyu"
alias install="sudo pacman -S --needed"
alias uninstall="sudo pacman -Rns"
alias cleanup="sudo pacman -Rns $(pacman -Qtdq)"

# files
alias open="sudo vim"
alias rmdir="sudo rm -rf"

# user
alias logout="i3-msg exit"

# file manager
alias la="ls -a"

# configs
alias i3config="sudo vim ~/.config/i3/config"
alias comptonconfig="sudo vim ~/.config/compton.conf"
alias polyconfig="sudo vim ~/.config/polybar/config"

# programms
alias androidstudio="cd /usr/local/android-studio/bin/ && ./studio.sh"

# raspberry
alias raspberry="sudo screen /dev/ttyACM0"

# java
alias jc="javac"
alias je="java"
