#############################################################################
###                                Colors		                  ###
#############################################################################

[colors]
;background = ${xrdb:color0:#222}
background = #dd232C31
background-alt = #ff232C31
foreground = ${xrdb:color7:#222}
foreground-alt = #555
primary = ${xrdb:color7:#222}
secondary = #e60053
alert = #bd2c40

#############################################################################
###                             Bar Settings                              ###
#############################################################################

[bar/bar]
monitor = ${env:MONITOR:DP-2}
width = 100%
height = 35
fixed-center = true

tray-position = right
tray-padding-right = 3
true tray-background = ${colors.background}

background = ${colors.background}
foreground = #ffffff

modules-left = i3
modules-center = date time
modules-right = spotify previous playpause next pulseaudio 

line-size = 3
line-color = #fff

padding-left = 0
padding-right = 2

module-margin-left = 2
module-margin-right = 2

cursor-click = pointer
enable-ipc = true

font-0 = Source Code Pro Medium:pixelsize=12;1
font-1 = FontAwesome5Free:pixelsize=12
font-2 = FontAwesome5Free:style=Solid:pixelsize=12
font-3 = FontAwesome5Brands:pixelsize=12
font-4 = FontAwesome5Brands:style=Solid:pixelsize=12

##############################################################################
###                              Modules                                   ###
##############################################################################

#
## i3 ##
#

[module/i3]
type = internal/i3
format = <label-state> <label-mode>
index-sort = true
wrapping-scroll = false

; Only show workspaces on the same output as the bar
;pin-workspaces = true

label-mode-padding = 2
label-mode-foreground = #000
label-mode-background = ${colors.background}

; focused = Active workspace on focused monitor
label-focused = %icon% %index%
label-focused-background = ${colors.background}
label-focused-underline = #f7d9ba
label-focused-padding = 2

; unfocused = Inactive workspace on any monitor
label-unfocused = %icon% %index%
label-unfocused-padding = 2

; visible = Active workspace on unfocused monitor
label-visible = %icon% %index%
label-visible-background = ${self.label-focused-background}
label-visible-underline = ${self.label-focused-underline}
label-visible-padding = ${self.label-focused-padding}

; urgent = Workspace with urgency hint set
label-urgent = %icon% %index%
label-urgent-background = ${colors.alert}
label-urgent-padding = 2

; Separator in between workspaces
; label-separator = |

ws-icon-0 = "1;"
ws-icon-1 = "2;"
ws-icon-2 = "3;"
ws-icon-3 = "4;"
ws-icon-default = 

#
## Date ##
#

[module/date]
type = internal/date
date = "%a %b %d"
date-alt = "%d/%m/%Y"
interval = 1
format-prefix = " "
label = "%date%"

#
## Time ##
#

[module/time]
type = internal/date
time = "%I:%M %p"
time-alt = "%I:%M %p"
interval = 1
format-prefix = " "
label = "%time%"

#
## Pulseaudio ##
#

[module/pulseaudio]
type = internal/pulseaudio
format-volume = <label-volume>
label-volume = %{T3} %{T1}%percentage%
;label-volume-foreground = ${root.foreground}
label-muted =  %percentage%
label-muted-foreground = ${colors.foreground-alt}
interval = 0.1

#
## Spotify ##
#

[module/previous]
type = custom/script
format = <label>
exec = echo ""
line-size = 1
click-left = "playerctl --player=spotify previous"

[module/next]
type = custom/script
format = <label>
label-padding-right = 4
exec = echo ""
line-size = 1
click-left = "playerctl --player=spotify next"

[module/playpause]
type = custom/ipc
hook-0 = echo ""
hook-1 = echo ""
hook-2 = echo ""
initial = 1
line-size = 1
click-left = "playerctl --player=spotify play-pause"

[module/spotify]
type = custom/script
format = <label>
format-prefix = " "
exec = ~/.config/polybar/scripts/spotify
interval = 0.1
