" ------------------------------------------------------------------------------
" Configuration for vim
" ------------------------------------------------------------------------------

" display settings
set encoding=utf-8 " encoding used for displaying file
set ruler " show the cursor position all the time
set number
set showmatch " highlight matching braces
set showmode " show insert/replace/visual mode

" write settings
set confirm " confirm :q in case of unsaved changes
set fileencoding=utf-8 " encoding used when saving file
set nobackup " do not keep the backup~ file

" edit settings
set backspace=indent,eol,start " backspacing over everything in insert mode
set nojoinspaces " no extra space after '.' when joining lines
set shiftwidth=8 " set indentation depth to 8 columns
set softtabstop=8 " backspacing over 8 spaces like over tabs
set tabstop=8 " set tabulator length to 8 columns
set textwidth=80 " wrap lines automatically at 80th column

" search settings
set hlsearch " highlight search results
set ignorecase " do case insensitive search...
set incsearch " do incremental search
set smartcase " ...unless capital letters are used

" file type specific settings
filetype on " enable file type detection
filetype plugin on " load the plugins for specific file types
filetype indent on " automatically indent code

set dir=~/.vim/tmp// " Put all swap files in common location
set viminfo+=n~/.vim/viminfo

" syntax highlighting
" colorscheme koehler " set color scheme, must be installed first
set background=dark " dark background for console
syntax enable " enable syntax highlighting

set cursorline
highlight clear CursorLine
highlight CursorLineNr cterm=bold

" characters for displaying non-printable characters
set listchars=eol:$,tab:>-,trail:.,nbsp:_,extends:+,precedes:+

" tuning for gVim only
if has('gui_running')
        set background=light " light background for GUI
        set columns=84 lines=48 " GUI window geometry
        set guifont=Monospace\ 12 " font for GUI window
        set number " show line numbers
endif
